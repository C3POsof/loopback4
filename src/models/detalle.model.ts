import {Entity, model, property} from '@loopback/repository';

@model()
export class Detalle extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  Detalle?: number;


  constructor(data?: Partial<Detalle>) {
    super(data);
  }
}

export interface DetalleRelations {
  // describe navigational properties here
}

export type DetalleWithRelations = Detalle & DetalleRelations;
